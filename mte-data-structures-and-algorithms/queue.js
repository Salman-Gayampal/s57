let collection = [];

// Write the queue functions below.
function print() {
	return collection;
}

function enqueue(value) {
	collection[collection.length] = value;
	return collection;
}

const dequeue = () => {
	collection.shift()
	return collection
}

const front = () => {
	return collection[0];
}

const size = () => {
	return collection.length;
}


const isEmpty = () => {
	return collection.length === 0;
}



module.exports = {
	collection,
	print,
	enqueue,
	dequeue,
	front,
	size,
	isEmpty
};